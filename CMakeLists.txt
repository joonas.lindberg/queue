cmake_minimum_required(VERSION 3.5)
project(queue)

set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS} -pthread")

enable_testing()

# Download and unpack googletest at configure time
configure_file(CMakeLists.txt.in googletest-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download)
if(result)
  message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download)
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/googletest-src
                 ${CMAKE_CURRENT_BINARY_DIR}/googletest-build
                 EXCLUDE_FROM_ALL)

include_directories(${CMAKE_SOURCE_DIR})

add_executable(test_data test/test_data.cpp)
set_property(TARGET test_data PROPERTY CXX_STANDARD 14)
target_link_libraries(test_data gtest_main gmock)
add_test(NAME test_data COMMAND test_data)

add_executable(test_circular_buffer test/test_circular_buffer.cpp)
set_property(TARGET test_circular_buffer PROPERTY CXX_STANDARD 14)
target_link_libraries(test_circular_buffer gtest_main gmock)
add_test(NAME test_circular_buffer COMMAND test_circular_buffer)

add_executable(data_rw example/data_rw.cpp)
set_property(TARGET data_rw PROPERTY CXX_STANDARD 14)

add_executable(pool example/pool.cpp)
set_property(TARGET pool PROPERTY CXX_STANDARD 14)

add_executable(pool_cargo_int example/pool_cargo_int.cpp)
set_property(TARGET pool_cargo_int PROPERTY CXX_STANDARD 14)

add_executable(test_h_list test/test_h_list.cpp)
set_property(TARGET test_h_list PROPERTY CXX_STANDARD 14)
target_link_libraries(test_h_list gtest_main gmock)
add_test(NAME test_h_list COMMAND test_h_list)
