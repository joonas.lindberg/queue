#pragma once

/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <cassert>
#include <algorithm>
#include <cstddef>

namespace kallkod {

namespace  implementation {
class list_node_base {
public:
    list_node_base* next = nullptr;
    list_node_base* previous = nullptr;

    list_node_base() = default;
    virtual ~list_node_base() = default;

    list_node_base(const list_node_base& ) = delete;
    list_node_base(const list_node_base&&) = delete;
    list_node_base& operator=(const list_node_base& ) = delete;
    list_node_base& operator=(const list_node_base&&) = delete;
};
}

class heterogeneous_list
{
public:
    heterogeneous_list() = default;
    ~heterogeneous_list();

    template <class T>
    void push_back(const T& value);

    template <class T, size_t N>
    using array = T[N];

    template <class T, size_t N>
    void push_back(const array<T, N>& value);

    template <class Y>
    const Y& front() const;

    bool empty() const;
    void pop_front();

private:
    using node = implementation::list_node_base;

    void append(node* n);
    node fakeNode;
    node* begin_ = &fakeNode;
    node* end_ = &fakeNode;
};

namespace  implementation {

template <class U>
class list_node : public list_node_base {
public:
    list_node(const U& u)
      : value(u)
    {
    }
    list_node() = default;
    U value;
};
}

inline heterogeneous_list::~heterogeneous_list()
{
    // TODO: destroy items properly
}

template <class T>
inline void heterogeneous_list::push_back(const T& value)
{
    auto* newNode = new implementation::list_node<T>(value);
    append(newNode);
}

template <class T, size_t N>
inline void heterogeneous_list::push_back(const heterogeneous_list::array<T, N>& value)
{
    auto* newNode = new implementation::list_node<array<T, N>>;
    std::copy(&value[0], &value[N], newNode->value);
    append(newNode);
}

inline void heterogeneous_list::append(node* n)
{
    if (begin_ == end_) // list is empty
    {
        begin_ = n;
        n->next = end_;
        end_->previous = n;
    }
    else
    {
        node* const oldLast = end_->previous;
        n->previous = oldLast;
        oldLast->next = n;

        end_->previous = n;
        n->next = end_;
    }
}

inline void heterogeneous_list::pop_front()
{
    assert(("Data container is empty", !empty()));    
    begin_ = begin_->next;
    begin_->previous = nullptr;
}

inline bool heterogeneous_list::empty() const
{
    return (begin_ == end_);
}

template <class Y>
inline const Y& heterogeneous_list::front() const
{
    // Should take the value from the very first node in the list.
    const auto& r = dynamic_cast<implementation::list_node<Y>&>(*begin_);
    return r.value;
}
}
