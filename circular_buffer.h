#pragma once

/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <cassert>
#include <memory>
#include <utility>
#include <type_traits>

namespace kallkod {
template <typename T>
class circular_buffer
{
public:
    circular_buffer() = default;
    explicit circular_buffer(size_t cap);
    explicit circular_buffer(const circular_buffer& src);
    const circular_buffer& operator=(const circular_buffer& src);
    explicit circular_buffer(circular_buffer&& src);
    circular_buffer& operator=(circular_buffer&& src);
    ~circular_buffer();

    void reserve(size_t cap);

    template<typename... Args>
    void emplace_back(Args&&... args);

    void push_back(const T& value);

    void pop_front();

    using RetType = typename std::conditional<std::is_scalar<T>::value, T, T&>::type;
    RetType front();
    RetType back();

    T&& retrieve();

    bool empty() const;
    bool full() const;

    size_t capacity() const { return capacity_; }
    size_t size() const { return count; }

    template <typename F>
    F for_each(F visitor);

    template <typename F>
    F for_each(F visitor) const;

private:
    void copy(const circular_buffer& src);
    void move(circular_buffer&& src);
    void free();

    template <typename F>
    void create_object(F creator);

    template <typename F, typename BufferRef>
    static F for_each_impl(F visitor, BufferRef& buffer);

    size_t capacity_ = 0;
    size_t count = 0;

    struct alignas(alignof(T)) MockType
    {
        char deadWeight[sizeof(T)];
    };

    T* storage = nullptr;
    T* beyond = nullptr;
    T* read_pos = nullptr;
    T* write_pos = nullptr;
};

template <typename T>
inline circular_buffer<T>::circular_buffer(size_t cap)
    : capacity_(cap)
{
    reserve(cap);
}

template <typename T>
inline circular_buffer<T>::circular_buffer(const circular_buffer<T>& src)
{
    copy(src);
}

template <typename T>
inline circular_buffer<T>::circular_buffer(circular_buffer<T>&& src)
{
    move(std::forward<circular_buffer<T>>(src));
}

template <typename T>
inline const circular_buffer<T> &circular_buffer<T>::operator=(const circular_buffer<T>& src)
{
    if (&src == this)
        return *this;

    free();
    copy(src);
    return *this;
}

template <typename T>
inline circular_buffer<T> &circular_buffer<T>::operator=(circular_buffer<T>&& src)
{
    if (&src == this)
        return *this;

    free();
    move(std::forward<circular_buffer<T>>(src));

    return *this;
}

template <typename T>
inline circular_buffer<T>::~circular_buffer()
{
    free();
}

template <typename T>
inline void circular_buffer<T>::reserve(size_t cap)
{
    assert(storage == nullptr);
    capacity_ = cap;
    MockType* memory = new MockType[capacity_];
    read_pos = write_pos = storage = reinterpret_cast<T*>(memory);
    beyond = storage + capacity_;
}

template <typename T>
template <typename F>
inline F circular_buffer<T>::for_each(F visitor)
{
    return for_each_impl(visitor, *this);
}

template <typename T>
template <typename F>
inline F circular_buffer<T>::for_each(F visitor) const
{
    return for_each_impl(visitor, *this);
}

template <typename T>
template <typename F, typename BufferRef>
inline F circular_buffer<T>::for_each_impl(F visitor, BufferRef& buffer)
{
    using Ptr = typename std::conditional<std::is_const<BufferRef>::value, const T*, T*>::type;
    Ptr ptr = buffer.read_pos;
    for (size_t i = 0; i < buffer.count; ++i)
    {
        visitor(ptr);
        ++ptr;
        if (ptr == buffer.beyond)
            ptr = buffer.storage;
    }

    return visitor;
}

template <typename T>
void circular_buffer<T>::copy(const circular_buffer &src)
{
    reserve(src.capacity_);
    auto inserter = [=](const T* v) { this->emplace_back(*v); };
    src.for_each(inserter);
}

template <typename T>
void circular_buffer<T>::move(circular_buffer&& src)
{
    storage   = src.storage;
    capacity_ = src.capacity_;
    count     = src.count;
    beyond    = src.beyond;
    read_pos  = src.read_pos;
    write_pos = src.write_pos;

    src.storage = nullptr;
    src.capacity_ = 0;
    src.count = 0;
    src.beyond = nullptr;
    src.read_pos = nullptr;
    src.write_pos = nullptr;
}

template <typename T>
void circular_buffer<T>::free()
{
    auto a = [](T* obj) { obj->~T(); };
    for_each(a);

    MockType* memory = reinterpret_cast<MockType*>(storage);
    delete[] memory;
}

template <typename T>
template <typename F>
inline void circular_buffer<T>::create_object(F creator)
{
    if (full())
        return;

    if (write_pos == beyond)
        write_pos = storage;

    creator(write_pos);

    ++count;
    if (write_pos < beyond)
    {
        ++write_pos;
    }
    if (write_pos == beyond && read_pos != storage)
        write_pos = storage;
}

template <typename T>
template <typename... Args>
inline void circular_buffer<T>::emplace_back(Args&&... args)
{
    auto a = [&](T* pos) { new (pos) T(std::forward<Args>(args)...); };

    create_object(a);
}

template <typename T>
inline void circular_buffer<T>::push_back(const T& value)
{
    auto a = [&](T* pos) { new (pos) T(value); };

    create_object(a);
}

template <typename T>
inline void circular_buffer<T>::pop_front()
{
    read_pos->T::~T();
    --count;

    ++read_pos;
    if (read_pos == beyond)
    {
        read_pos = storage;
        if (write_pos == beyond)
            write_pos = storage;
    }
}

template <typename T>
inline typename circular_buffer<T>::RetType circular_buffer<T>::front()
{
    return *read_pos;
}

template <typename T>
inline typename circular_buffer<T>::RetType circular_buffer<T>::back()
{
    assert(("Data container is empty", !empty()));
    return *(((write_pos == storage) ? beyond : write_pos) - 1);
}

template <typename T>
inline T&& circular_buffer<T>::retrieve()
{
    return std::move(*read_pos);
}

template <typename T>
inline bool circular_buffer<T>::empty() const
{
    return count == 0;
}

template <typename T>
inline bool circular_buffer<T>::full() const
{
    return count == capacity_;
}

} // namespace kallkod
