/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <iostream>
#include <chrono>
#include <thread>
#include <atomic>
#include <condition_variable>
#include "queue.h"
#include <list>
#include "circular_buffer.h"

using namespace std;
using namespace kallkod;
//typedef int Type;

struct Cargo
{
    int data[256];

    Cargo(int n)
    {
        data[0] = n;
    }

    operator int() const
    {
        return data[0];
    }

    Cargo& operator++()
    {
        data[0] += 1;
        return *this;
    }
}; typedef Cargo Type;

template <class T, class Unused>
using BufferAdapter = circular_buffer<T>;
using Queue = kallkod::queue<Type, BufferAdapter>;
//using Queue = Kallkod::Queue<Type>;

void read_print(Queue &d, unsigned lim)
{
    while (lim)
    {
        auto x = d.size();
        auto y = d.receive();
        auto z = d.size();
        //cout << "received value: " << y << ", vector size: " << z << endl;
        lim -= 1;
    }
}

void write_print(Type x, Queue &d, unsigned count)
{
    while (count)
    {
        d.send(++x);
        count -= 1;
    }
}

void print_last_value(Queue &d)
{
    cout << "Last vector value: " << d.get_last_value() << endl;
}

auto start()
{
    auto start = chrono::high_resolution_clock::now();
    return start;
}

auto stop()
{
    auto stop = chrono::high_resolution_clock::now();
    return stop;
}

template <typename T>
void duration(T start, T stop)
{
    unsigned int time_interval = chrono::duration_cast<chrono::milliseconds>(stop - start).count();
    cout << "Time calculaton (vector): " << time_interval << " milliseconds" << endl;
}

int main(int argc, char ** argv)
{
    unsigned lim{int(5e3)};
    Type N = 1e6;

    Queue D(lim);

    int m = 77;
    auto a = [&]()
    {
        return ++m;
    };

    auto start_t = start();

    thread sender1(&write_print, m, ref(D), N);
    thread sender2(&write_print, m, ref(D), N);
    thread reader(&read_print, ref(D), N*2);

    sender1.join();
    cout << "sender1 done." << endl;
    sender2.join();
    cout << "sender2 done." << endl;
    reader.join();
    cout << "reader done." << endl;

    auto stop_t = stop();

    duration(start_t, stop_t);

    return 0;
}
