/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "heterogeneous_list.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>

using kallkod::heterogeneous_list;

TEST(HList, DefaultConstructor)
{
    heterogeneous_list uut;
    (void)uut;
}

struct S1
{
    long double d;
    char c[11];
};

TEST(HList, push_back)
{
    heterogeneous_list uut;
    uut.push_back(32767);
    uut.push_back(std::string("hello world"));
    uut.push_back("another literal");
    uut.push_back(S1{});
}

TEST(HList, front)
{
    heterogeneous_list uut;
    uut.push_back(321);
    ASSERT_EQ(321, uut.front<int>());
}

TEST(HList, empty)
{
    heterogeneous_list uut;
    ASSERT_TRUE(uut.empty());
    uut.push_back(0);
    ASSERT_FALSE(uut.empty());
    uut.pop_front();
    ASSERT_TRUE(uut.empty());
}

TEST(HList, pop_front)
{
    heterogeneous_list uut;
    uut.push_back(32767);
    uut.push_back(std::string("hello world"));
    uut.push_back("another literal");
    uut.push_back(S1{});
    uut.pop_front();
    uut.pop_front();
    uut.pop_front();
    uut.pop_front();
    ASSERT_TRUE(uut.empty());
}
