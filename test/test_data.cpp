/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include "queue.h"
#include "circular_buffer.h"
#include <iostream>

using namespace kallkod;

struct Sv
{
    int x;
    std::string y;
};

template <class T, class Unused>
using BufferAdapter = circular_buffer<T>;
template <class T>
using CircularQueue = kallkod::queue<T, BufferAdapter>;

TEST(queue, Default_constructor)
{
    queue<Sv> d1(0);
    queue<Sv> d2(5);

    ASSERT_FALSE(d1.capacity());
    ASSERT_TRUE(d2.capacity());
    ASSERT_EQ(d2.size(), 0);

    CircularQueue<Sv> d3(5);
    ASSERT_EQ(d3.capacity(), 5);
    ASSERT_EQ(d3.size(), 0);
}

TEST(queue, Check_limit)
{
    queue<int> d1(0);
    queue<int> d2(7);

    ASSERT_TRUE(d1.capacity() == 0);
    ASSERT_TRUE(d2.capacity() == 7);
}

TEST(queue, Write_function)
{
    queue<int> d(2);
    d.send(42);
    ASSERT_EQ(d.size(), 1);
    ASSERT_EQ(d.receive(), 42);
}

TEST(queue, TrySend)
{
    queue<int> d(1);
    ASSERT_TRUE(d.try_send(1));
    ASSERT_FALSE(d.try_send(20));
}

TEST(queue, Read_function)
{
    queue<int> d(1);
    d.send(1);
    d.receive();
}

TEST(queue, Read_number)
{
    queue<int> d(1);
    d.send(1);
    ASSERT_TRUE(d.receive() == 1);
}

TEST(queue, Read_numbers)
{
    queue<int> d(3);
    d.send(1);
    d.send(2);
    d.send(3);
    ASSERT_TRUE(d.receive() == 1);
    ASSERT_TRUE(d.receive() == 2);
    ASSERT_TRUE(d.receive() == 3);
}

TEST(queue, Fill)
{
    queue<int> d(2);

    auto a = [&]()
    {
     return 42;
    };

    d.fill(a);
    ASSERT_NE(d.receive(), 420);
    ASSERT_EQ(d.receive(), 42);
}

TEST(queue, Fill_n)
{
    queue<int> d(2);

    auto a = [&]()
    {
     return 42;
    };

    d.fill(a, 1);
    ASSERT_EQ(d.size(), 1);
    ASSERT_EQ(d.receive(), 42);
}

TEST(queue, Get_last_value)
{
    queue<int> d(2);

    auto a = [&]()
    {
     return 42;
    };

    d.fill(a, 1);
    ASSERT_EQ(d.get_last_value(), 42);
}
