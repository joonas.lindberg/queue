/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "circular_buffer.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>

using int_circular_buffer = kallkod::circular_buffer<int>;

inline void fill(int_circular_buffer& b)
{
    for (unsigned x = 0; x < b.capacity(); ++x)
        b.push_back(static_cast<int>(x));
}

TEST(CircularBuffer, DefaultConstructor)
{
    int_circular_buffer uut(1);
    ASSERT_TRUE(uut.empty());
}

TEST(CircularBuffer, SinglePush)
{
    int_circular_buffer uut(1);

    uut.push_back(1);
    ASSERT_FALSE(uut.empty());
}

TEST(CircularBuffer, Fill)
{
    int_circular_buffer uut(7);

    fill(uut);

    ASSERT_FALSE(uut.empty());
    ASSERT_TRUE(uut.full());
}

TEST(CircularBuffer, Back)
{
    int_circular_buffer uut(2);

    uut.push_back(1);
    ASSERT_EQ(uut.back(), 1);
    uut.push_back(2);
    ASSERT_EQ(uut.back(), 2);
    uut.push_back(3);
    ASSERT_EQ(uut.back(), 2);
}

TEST(CircularBuffer, Back3)
{
    int_circular_buffer uut(3);

    uut.push_back(1);
    uut.push_back(20);
    uut.pop_front();
    ASSERT_EQ(uut.front(), 20);
    uut.push_back(30);
    ASSERT_EQ(uut.back(), 30);
    uut.push_back(40);
    ASSERT_EQ(uut.back(), 40);
    uut.push_back(42);
    ASSERT_EQ(uut.back(), 40);
}

TEST(CircularBuffer, PushAndPop)
{
    int_circular_buffer uut(11);

    constexpr int MAGIC = 199;
    uut.push_back(MAGIC);
    auto v = uut.front();
    uut.pop_front();
    ASSERT_EQ(MAGIC, v);
    ASSERT_TRUE(uut.empty());
    ASSERT_FALSE(uut.full());
}

TEST(CircularBuffer, PushPopFill)
{
    int_circular_buffer uut(13);

    uut.push_back(1);
    uut.pop_front();

    fill(uut);

    ASSERT_FALSE(uut.empty());
    ASSERT_TRUE(uut.full());
}

TEST(CircularBuffer, FillPopPush)
{
    int_circular_buffer uut(16);

    fill(uut);

    ASSERT_FALSE(uut.empty());
    ASSERT_TRUE(uut.full());

    auto x = uut.front();
    uut.pop_front();
    ASSERT_EQ(0, x);
    ASSERT_FALSE(uut.empty());
    ASSERT_FALSE(uut.full());

    uut.push_back(1000);
    ASSERT_FALSE(uut.empty());
    ASSERT_TRUE(uut.full());
}

struct payload {
    std::string s;
    payload(std::string p)
        : s{p}
    {}

    payload(payload&& p) = default;
    payload(const payload& p) = default;

    payload& operator=(const payload& p) = delete;
};

bool operator==(const payload& a, const payload& b)
{
    return a.s == b.s;
}
using string_buffer = kallkod::circular_buffer<payload>;

TEST(CircularBuffer, PushAndPopStruct)
{
    string_buffer uut(1);

    const payload MAGIC{ "asdf" };
    uut.push_back(MAGIC);
    auto v = uut.front();
    uut.pop_front();
    ASSERT_EQ(MAGIC, v);
    ASSERT_TRUE(uut.empty());
    ASSERT_FALSE(uut.full());
}

TEST(CircularBuffer, BackStruct)
{
    string_buffer uut(1);

    const payload MAGIC{ "qwerty" };
    uut.push_back(MAGIC);
    auto v{uut.back()};
    ASSERT_EQ(MAGIC, v);
}

TEST(CircularBuffer, EmplaceBack)
{
    string_buffer uut(1);

    const payload MAGIC{ "asdf" };
    uut.emplace_back(MAGIC.s);
    ASSERT_FALSE(uut.empty());
    ASSERT_TRUE(uut.full());

    auto v = uut.front();
    uut.pop_front();
    ASSERT_EQ(MAGIC, v);
    ASSERT_TRUE(uut.empty());
    ASSERT_FALSE(uut.full());
}

struct counter
{
    MOCK_METHOD0(plus_one, void());
};

struct checked_construction
{
    checked_construction(counter& c)
    {
        c.plus_one();
    }

    checked_construction(checked_construction&&) = delete;
};
using count_construction_buffer = kallkod::circular_buffer<checked_construction>;

TEST(CircularBuffer, ConstructOneObject)
{
    counter counter;
    EXPECT_CALL(counter, plus_one()).Times(1);

    count_construction_buffer uut(111);
    uut.emplace_back(counter);
}

TEST(CircularBuffer, FillBuffer)
{
    counter counter;
    EXPECT_CALL(counter, plus_one()).Times(4);

    count_construction_buffer uut(4);
    while (!uut.full())
        uut.emplace_back(counter);
}

TEST(CircularBuffer, EmplacePopAndFillBuffer)
{
    counter counter;
    EXPECT_CALL(counter, plus_one()).Times(5);

    count_construction_buffer uut(4);
    uut.emplace_back(counter);
    (void)uut.pop_front();
    while (!uut.full())
        uut.emplace_back(counter);
}

struct checked_destroy
{
    counter* ptr;

    checked_destroy(counter* p)
        : ptr(p)
    {}

    checked_destroy(checked_destroy&& orig) noexcept
        : ptr(orig.ptr)
    {
        orig.ptr = nullptr;
    }

    ~checked_destroy()
    {
        if (ptr)
        {
            ptr->plus_one();
        }
    }
};
using checked_buffer = kallkod::circular_buffer<checked_destroy>;

TEST(CircularBuffer, ReleaseCount1)
{
    counter counter;
    EXPECT_CALL(counter, plus_one()).Times(1);

    checked_buffer uut(1);
    uut.emplace_back(&counter);

    // destructor of CircularBuffer should destroy all contained objects
}

TEST(CircularBuffer, EmplaceAndRelease)
{
    counter counter;
    EXPECT_CALL(counter, plus_one()).Times(1);

    {
        checked_buffer uut(100);
        uut.emplace_back(&counter);
        checked_destroy tmp{ uut.retrieve() };
        uut.pop_front();
    }
}

TEST(CircularBuffer, FillAndRelease)
{
    counter counter;
    EXPECT_CALL(counter, plus_one()).Times(4);
    {
        checked_buffer uut(4);
        while (!uut.full())
            uut.emplace_back(&counter);

        // destructor of CircularBuffer should destroy all contained objects
    }
}

TEST(CircularBuffer, ReleaseCount6)
{
    counter counter;
    // 1 object is emplaced and removed,
    // and then another 4 are added to container
    EXPECT_CALL(counter, plus_one()).Times(1 + 4);

    {
        checked_buffer uut(4);
        {
            uut.emplace_back(&counter);
            checked_destroy tmp{ uut.retrieve() };
            uut.pop_front();
        }
        while (!uut.full())
            uut.emplace_back(&counter);

        // destructor of CircularBuffer should destroy all contained objects
    }
}

struct accumulator
{
    int summa = 0;

    void operator()(int* p) { summa += *p; }
};

TEST(CircularBuffer, ForEach)
{
    constexpr int COUNT = 51;
    int_circular_buffer uut(COUNT);

    for (int n = 0; !uut.full(); ++n)
        uut.push_back(n);

    auto a = uut.for_each(accumulator());
    ASSERT_EQ(a.summa, COUNT * (COUNT - 1) / 2);
}

TEST(CircularBuffer, Copy)
{
    int_circular_buffer original(1);
    original.push_back(11);
    int_circular_buffer uut(original);

    ASSERT_EQ(uut.capacity(), original.capacity());
    ASSERT_EQ(uut.front(), original.front());
}

struct checked_copy
{
    checked_copy(counter& cnt)
        : c(cnt)
    {
    }

    checked_copy(const checked_copy& src)
      : c(src.c)
    {
        c.plus_one();
    }

    checked_copy(checked_copy&&) = delete;
    counter& c;
};

using count_copy_buffer = kallkod::circular_buffer<checked_copy>;

TEST(CircularBuffer, CopyStruct)
{
    counter cnt;

    count_copy_buffer original(5);
    while (!original.full())
        original.emplace_back(cnt);

    EXPECT_CALL(cnt, plus_one()).Times(original.capacity());
    count_copy_buffer uut(original);
    ASSERT_EQ(uut.capacity(), original.capacity());
}

TEST(CircularBuffer, Copy_Assign)
{
    int_circular_buffer original(2);
    original.push_back(11);

    int_circular_buffer uut;

    uut = original;

    ASSERT_EQ(uut.capacity(), original.capacity());
    ASSERT_EQ(uut.front(), original.front());

    ASSERT_EQ(original.back(), 11);
    ASSERT_EQ(uut.back(), 11);

    original.push_back(111);
    uut.push_back(111);

    ASSERT_EQ(original.back(), uut.back());
}

TEST(CircularBuffer, Move)
{
    int_circular_buffer original(2);
    original.push_back(1);
    original.push_back(11);
    ASSERT_EQ(original.front(), 1);
    ASSERT_EQ(original.back(), 11);

    int_circular_buffer uut(std::move(original));

    ASSERT_EQ(uut.capacity(), 2);

    ASSERT_EQ(uut.front(), 1);
    ASSERT_EQ(uut.back(), 11);
}

TEST(CircularBuffer, Move_Assign)
{
    int_circular_buffer original(2);
    original.push_back(1);
    original.push_back(11);
    ASSERT_EQ(original.front(), 1);
    ASSERT_EQ(original.back(), 11);

    int_circular_buffer uut;
    uut = std::move(original);

    ASSERT_EQ(uut.capacity(), 2);

    ASSERT_EQ(uut.front(), 1);
    ASSERT_EQ(uut.back(), 11);
}

TEST(CircularBuffer, Move_FreeAndAssign)
{
    int_circular_buffer original(2);
    original.push_back(1);
    original.push_back(11);
    ASSERT_EQ(original.front(), 1);
    ASSERT_EQ(original.back(), 11);

    int_circular_buffer uut(4);
    uut.push_back(2);
    uut.push_back(22);
    uut = std::move(original);

    ASSERT_EQ(uut.capacity(), 2);

    ASSERT_EQ(uut.front(), 1);
    ASSERT_EQ(uut.back(), 11);
}

TEST(CircularBuffer, Move_FreeAndAssignStruct)
{
    counter o;
    EXPECT_CALL(o, plus_one()).Times(1);
    counter u;
    EXPECT_CALL(u, plus_one()).Times(1);

    checked_buffer original(1);
    original.emplace_back(&o);

    checked_buffer uut(5);
    uut.emplace_back(&u);

    uut = std::move(original);
}
